# OpenAPI Generator for Serverless Lambda projects

## Overview
Generate that creates a serverless project that contains a lambda function for each service (equivalent to tag in OpenApi)

## Usage
The repository consits of the following files:

```
.
|- README.md    // this file
|- pom.xml      // build script
|-- src
|--- main
|---- java
|----- nl.researchable.serverless.codegen.ServerlessLambdaRubyCodegen.java // generator file
|----- nl.researchable.serverless.codegen.DefaultGenerator.java // override openapi generator
|---- resources
|----- serverless-lambda-ruby // template files
|----- META-INF
|------ services
|------- org.openapitools.codegen.CodegenConfig
```

In orde to package the generator you can run:

```
mvn [clean] package [-Dskiptests]
```

In the root of the repository. A single jar file will be produced in `target`. You can now use that with [OpenAPI Generator](https://openapi-generator.tech):

Generate Serverless Lambda project:
```
java -cp target/serverless-lambda-nodejs-openapi-generator-1.0.0.jar:openapi-generator-cli.jar org.openapitools.codegen.OpenAPIGenerator generate -g serverless-lambda-nodejs -i <path-to-openapi-file> -o <output-directory> [--skip-overwrite]

```
