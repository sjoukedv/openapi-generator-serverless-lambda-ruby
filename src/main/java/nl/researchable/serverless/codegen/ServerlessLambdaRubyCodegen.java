package nl.researchable.serverless.codegen;

import org.openapitools.codegen.*;
import io.swagger.models.properties.*;
import org.openapitools.codegen.config.GlobalSettings;
import org.openapitools.codegen.meta.GeneratorMetadata;
import org.openapitools.codegen.meta.Stability;
import org.openapitools.codegen.meta.features.*;
import org.openapitools.codegen.DefaultGenerator;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.openapitools.codegen.utils.StringUtils.camelize;
import static org.openapitools.codegen.utils.StringUtils.underscore;
import java.util.*;
import java.io.File;

import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;

public class ServerlessLambdaRubyCodegen extends DefaultCodegen implements CodegenConfig {
  private final Logger LOGGER = LoggerFactory.getLogger(ServerlessLambdaRubyCodegen.class);

  protected String apiVersion = "1.0.0";
  protected String implFolder = "services";
  protected String projectName = "serverless-lambda-ruby";

  public ServerlessLambdaRubyCodegen() {
    super();

    generatorMetadata = GeneratorMetadata.newBuilder(generatorMetadata)
                .stability(Stability.BETA)
                .build();

    // taken from nodejs-express-server
    setReservedWordsLowerCase(
            Arrays.asList(
                    "break", "case", "class", "catch", "const", "continue", "debugger",
                    "default", "delete", "do", "else", "export", "extends", "finally",
                    "for", "function", "if", "import", "in", "instanceof", "let", "new",
                    "return", "super", "switch", "this", "throw", "try", "typeof", "var",
                    "void", "while", "with", "yield")
    );

    outputFolder = "generated-code/serverless-lambda-ruby";
    embeddedTemplateDir = templateDir = "serverless-lambda-ruby";

    additionalProperties.put("apiVersion", apiVersion);
    additionalProperties.put("implFolder", implFolder);

    // Api classes.  Generates lambda function handlers
    apiPackage = "functions";
    apiTemplateFiles.put("functions.handler.mustache", ".rb");  
    apiTemplateFiles.put("functions.mustache", ".rb");   
    
    // no model file
    // modelTemplateFiles.clear();
    modelPackage = "layers" + File.separator + "ruby" + File.separator + "lib" ;
    modelTemplateFiles.put("models.mustache", ".rb");   
    
    // serverless files
    supportingFiles.add(new SupportingFile("package.json", "", "package.json").doNotOverwrite());
    supportingFiles.add(new SupportingFile("serverless.mustache", "", "serverless.yml"));
    // supportingFiles.add(new SupportingFile(".env", "", ".env")); 
    supportingFiles.add(new SupportingFile(modelPackage + File.separator + "Gemfile", "", "Gemfile").doNotOverwrite());

    // lambda layers
    // supportingFiles.add(new SupportingFile(modelPackage + File.separator + "Gemfile", modelPackage, "Gemfile").doNotOverwrite());
    // supportingFiles.add(new SupportingFile(modelPackage + File.separator + "index.mustache", modelPackage, "index.rb")); 
    supportingFiles.add(new SupportingFile(modelPackage + File.separator + "object.rb", modelPackage, "object.rb")); 
    supportingFiles.add(new SupportingFile(modelPackage + File.separator + "integer.rb", modelPackage, "integer.rb")); 
  }

  // TODO clean method
  // @Override
  public String apiFilename(String templateName, String tag) {
    // split[0] = tag
    // split[1] = operationId
    String[] split = tag.split(",");
    
    String suffix = apiTemplateFiles().get(templateName);
    if(split.length < 2){
      if(templateName == "functions.handler.mustache") return apiFileFolder() + File.separator + toApiFilename(split[0]) + File.separator + "handler" + suffix;
      // we don't need these files
      return apiFileFolder() + File.separator + "output.rb.ignore";

    } 

    if(templateName == "functions.handler.mustache") return apiFileFolder() + File.separator + "output.rb.ignore";
    if(templateName == "functions.mustache") return apiFileFolder() + File.separator + toApiFilename(split[0]) + File.separator + split[1] + suffix;
    // default return value
    return apiFileFolder() + File.separator + split[0] + File.separator + toApiFilename(split[1]) + suffix;
  }

  /**
   * Configures the type of generator.
   * 
   * @return  the CodegenType for this generator
   * @see     org.openapitools.codegen.CodegenType
   */
  public CodegenType getTag() {
    return CodegenType.SERVER;
  }

  /**
   * Configures a friendly name for the generator.  This will be used by the generator
   * to select the library with the -g flag.
   * 
   * @return the friendly name for the generator
   */
  public String getName() {
    return "serverless-lambda-ruby";
  }

  /**
   * Returns human-friendly help for the generator.  Provide the consumer with help
   * tips, parameters here
   * 
   * @return A string value for the help message
   */
  public String getHelp() {
    return "Generates a serverless-lambda-ruby serverless library.";
  }

  /**
   * Escapes a reserved word as defined in the `reservedWords` array. Handle escaping
   * those terms here.  This logic is only called if a variable matches the reserved words
   * 
   * @return the escaped term
   */
  @Override
  public String escapeReservedWord(String name) {
    return "_" + name;  // add an underscore to the name
  }

  /**
   * Location to write model files.  You can use the modelPackage() as defined when the class is
   * instantiated
   */
  public String modelFileFolder() {
    return outputFolder + "/" + modelPackage().replace('.', File.separatorChar);
  }

  @Override
  public String apiPackage() {
      return "functions";
  }

  @Override
  public String toApiName(String name) {
      if (name.length() == 0) {
          return "Default";
      }
      return camelize(name);
  }

  /**
   * Location to write api files.  You can use the apiPackage() as defined when the class is
   * instantiated
   */
  @Override
  public String apiFileFolder() {
      return outputFolder + File.separator + apiPackage().replace('.', File.separatorChar);
  }

  /**
   * override with any special text escaping logic to handle unsafe
   * characters so as to avoid code injection
   *
   * @param input String to be cleaned up
   * @return string with unsafe characters removed or escaped
   */
  @Override
  public String escapeUnsafeCharacters(String input) {
    //TODO: check that this logic is safe to escape unsafe characters to avoid code injection
    return input;
  }

  /**
   * Escape single and/or double quote to avoid code injection
   *
   * @param input String to be cleaned up
   * @return string with quotation mark removed or escaped
   */
  public String escapeQuotationMark(String input) {
    //TODO: check that this logic is safe to escape quotation mark to avoid code injection
    return input.replace("\"", "\\\"");
  }

  @Override
  public void preprocessOpenAPI(OpenAPI openAPI) {
      if (openAPI.getInfo() != null) {
          Info info = openAPI.getInfo();
          if (info.getTitle() != null) {
              // when info.title is defined, use it for projectName
              // used in package.json
              projectName = info.getTitle()
                      .replaceAll("[^a-zA-Z0-9]", "-")
                      .replaceAll("^[-]*", "")
                      .replaceAll("[-]*$", "")
                      .replaceAll("[-]{2,}", "-")
                      .toLowerCase(Locale.ROOT);
              this.additionalProperties.put("projectName", projectName);
          }
      }
  }

  @Override
  // override with any message to be shown right before the process finishes
  @SuppressWarnings("static-method")
  public void postProcess() {
    // remove annoying printing
  }
}
